package Contest;

//abstract class mengimplementasikan interface HargaTiket
//dalam class TiketKonser harus mengimplementasikan semua method yang ada di dalam class HargaTiket
abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    // men-declare variable
    private String tiket;
    private int harga;

    // constructor
    public TiketKonser(String tiket, int harga) {
        this.tiket = tiket;
        this.harga = harga;
    }

    // getter
    public String getTiket() {
        return tiket;
    }

    public int getHarga() {
        return harga;
    }
}