
/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        // Do your magic here...
        Scanner x = new Scanner(System.in);

        // try-catch digunakan untuk menangani kesalahan yang mungkin dapat terjadi
        // ketika program dieksekusi
        try {
            System.out.println("Selamat datang di laman pemesanan tiket Coldplay!\n");
            System.out.print("Masukkan nama : ");
            String nama = x.nextLine();

            // menampilkan daftar menu atau pilihan tiket
            System.out.println("Pilih jenis tiket:");
            System.out.println("1. CAT 8");
            System.out.println("2. CAT 1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. UNLIMITED EXPERIENCE");
            System.out.print("Masukkan pilihan : ");
            int pilihan = x.nextInt();

            // memberikan kondisi jika pilihan kurang dari 1 dan lebih dari 5, maka akan
            // muncul exception handling
            if (pilihan < 1 || pilihan > 5) {
                // satu baris kode di bawah ini akan muncul jika input memenuhi kondisi diatas
                throw new InvalidInputException("Tiket tidak valid. Harap masukkan nomor antara 1 hingga 5");
            }

            TiketKonser jenistiket;
            switch (pilihan) {
                case 1:
                    jenistiket = new CAT8();
                    break;
                case 2:
                    jenistiket = new CAT1();
                    break;
                case 3:
                    jenistiket = new FESTIVAL();
                    break;
                case 4:
                    jenistiket = new VIP();
                    break;
                case 5:
                    jenistiket = new VVIP();
                    break;
                default:
                    // jika input berupa huruf atau karakter selain angka 1-5 maka akan muncul pesan
                    // exception
                    throw new InvalidInputException("Terjadi kesalahan dalam memilih tiket");
            }

            String kodePemesanan = generateKodeBooking();
            String tanggalPemesanan = getCurrentDate();

            System.out.println("\n----- Detail Pemesanan -----");
            System.out.println("Nama Pemesan       : " + nama);
            System.out.println("Kode Booking       : " + kodePemesanan);
            System.out.println("Tanggal Pemesanan  : " + tanggalPemesanan);
            System.out.println("Tiket yang dipesan : " + jenistiket.getTiket());
            System.out.println("Total Harga        : " + jenistiket.getHarga());
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan : " + e.getMessage());
        }

    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}