package Contest;

import java.util.Date;
import java.text.SimpleDateFormat;

class PemesananTiket {
    // Do your magic here...
    //men-declare variabel dari class pemesananTiket
    private String nama; //nama yang memesan tiket
    private String kodePemesanan;
    private Date tanggalPemesanan; //tanggal kapan tiket tersebut dibeli
    private String tiket; //jenis-jenis tiket
    private int harga;

    //membuat constructor 
    public PemesananTiket(String nama, String tiket, int harga) {
        this.nama = nama;
        this.tiket = tiket;
        this.harga = harga;
        this.tanggalPemesanan = new Date();
        this.kodePemesanan = generateKodeBooking();

    }

    //getter
    public String getNama() {
        return nama;
    }

    public String getTiket() {
        return namaTiket;
    }

    public int getHarga() {
        return harga;
    }

    public Date getTanggalPemesanan() {
        return tanggalPemesanan;
    }

    public String getKodePemesanan() {
        return kodePemesanan;
    }

}