package Contest;

//class turunan dari class TiketKonser
class VIP extends TiketKonser {
    // Do your magic here...
    public VIP() {
        // memanggil contructor superclass yang ada di class TiketKonser
        super("VIP", 3500000);
    }
}