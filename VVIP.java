package Contest;

//class turunan dari class TiketKonser
class VVIP extends TiketKonser {
    // Do your magic here...
    public VVIP() {
        // memanggil contructor superclass yang ada di class TiketKonser
        super("UNLIMITED EXPERIENCE", 11000000);
    }
}